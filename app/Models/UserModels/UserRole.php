<?php

namespace App\Models\UserModels;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserRole extends Pivot
{
    /** @var string */
    protected $table = 'role_user';
}
