<?php

namespace App\Models\PageModels;

use Illuminate\Database\Eloquent\Model;

class PageType extends Model
{
    #region CONSTANTS
    /** pages type that is included into main menus */
    public const MENU = 1;

    /** pages types that belong to "content" (one news, one article, one blog post etc) */
    public const NEWS = 2;
    public const PROJECT = 3;
    public const SERVICE = 4;

    public const SINGLE = 5;
    #endregion

    #region CLASS PROPERTIES
    protected $table = 'page_types';
    protected $guarded = [];
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    public function pages()
    {
        return $this->hasMany(Page::class, 'page_type_id', 'id');
    }
    #endregion
}
