<?php

namespace App\Models\RequestModels;

use Illuminate\Database\Eloquent\Model;

class ContactRequest extends Model
{
    #region CLASS PROPERTIES
    protected $table = 'contact_requests';
    protected $fillable = ['*'];
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    #endregion
}
