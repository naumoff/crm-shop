<?php
/**
 * User: Andrey Naumoff
 * Date: 31-Dec-18
 * Time: 4:39 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\DataHelpers;

/**
 * Trait MenuPageBooleans
 * Used only during data migration to db
 * @package App\Services\DataHelpers
 */
trait MenuPageBooleans
{
    #region MAIN PAGE
    protected $mainPageBooleans = [
        [
            'name' => 'showSlider',
            'description' => 'first slider block show/hide',
            'value' => true
        ],
        [
            'name' => 'showExperience',
            'description' => 'experience block show/hide',
            'value' => true
        ],
        [
            'name' => 'showBrochureButton',
            'description' => '\'get Brochure\' button in experience block show/hide',
            'value' => true
        ],
        [
            'name' => 'showCounters',
            'description' => 'project counters block show/hide',
            'value' => true
        ],
        [
            'name' => 'showOurProjects',
            'description' => 'our projects block show/hide',
            'value' => true
        ],
        [
            'name' => 'showWhyChooseUs',
            'description' => 'why choose us block show/hide',
            'value' => true
        ],
        [
            'name' => 'showOurProfessionals',
            'description' => 'team block show/hide',
            'value' => true
        ],
        [
            'name' => 'showOurClients',
            'description' => 'clients block show/hide',
            'value' => true
        ],
        [
            'name' => 'showNews',
            'description' => 'news block show/hide',
            'value' => true
        ],
    ];
    #endregion

    #region ABOUT US PAGE
    protected $aboutUsPageBooleans = [
        [
            'name' => 'showBreadCrumbs',
            'description' => 'bread crumbs show/hide',
            'value' => true
        ],
        [
            'name' => 'showExperience',
            'description' => 'experience block show/hide',
            'value' => true
        ],
        [
            'name' => 'showHistory',
            'description' => 'history block show/hide',
            'value' => true
        ],
        [
            'name' => 'showTargetAndCertificates',
            'description' => 'target and certificates block show/hide',
            'value' => true
        ],
        [
            'name' => 'showTopManagement',
            'description' => 'top management block show/hide',
            'value' => true
        ],
    ];
    #endregion

    #region SERVICES
    protected $servicesPageBooleans = [
        [
            'name' => 'showBreadCrumbs',
            'description' => 'bread crumbs show/hide',
            'value' => true
        ],
        [
            'name' => 'showOurProduction',
            'description' => 'our production block show/hide',
            'value' => true
        ],
        [
            'name' => 'showCounter',
            'description' => 'our production block show/hide',
            'value' => true
        ],
    ];
    #endregion

    #region OUR WORKS PAGE
    protected $ourWorkPageBooleans = [
        [
            'name' => 'showBreadCrumbs',
            'description' => 'bread crumbs show/hide',
            'value' => true
        ],
    ];
    #endregion

    protected $newsPageBooleans = [
        [
            'name' => 'showBreadCrumbs',
            'description' => 'bread crumbs show/hide',
            'value' => true
        ],
    ];

    #region CONTACTS PAGE
    protected $contactsPageBooleans = [
        [
            'name' => 'showBreadCrumbs',
            'description' => 'bread crumbs show/hide',
            'value' => true
        ],
        [
            'name' => 'showContactForm',
            'description' => 'contact form block show/hide',
            'value' => true
        ],
        [
            'name' => 'showSocialLinks',
            'description' => 'social links show/hide',
            'value' => true
        ],
        [
            'name' => 'showPhoneNumbers',
            'description' => 'phone numbers show/hide',
            'value' => true
        ],
        [
            'name' => 'showEmailAddress',
            'description' => 'email address show/hide',
            'value' => true
        ],
        [
            'name' => 'showPhysicalAddress',
            'description' => 'physical address show/hide',
            'value' => true
        ],
        [
            'name' => 'showGoogleMap',
            'description' => 'google Map show/hide',
            'value' => true
        ],
    ];
    #endregion
}
