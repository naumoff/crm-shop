<?php
/**
 * User: Andrey Naumoff
 * Date: 24-Dec-18
 * Time: 1:56 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\DataHelpers;

use App\Models\PageModels\Page;
use App\Services\Converters\DashConverters;
use LogRec;
use Route;

trait MenuPageRoutes
{
    use MenuPages, DashConverters;

    #region TRAIT METHODS
    /**
     * ugly way to set one value to two variables
     * @param string $property
     * @return array|null
     */
    public function __get(string $property): ?array
    {
        if ($property === 'menuPageRoutes') {
            return $this->menuPageNames;
        };
        return null;
    }

    /**
     * @return array
     */
    protected function convertMenuPageRoutesToVarsArray(): array
    {
        $menuPagesNames = [];
        foreach ($this->menuPageRoutes as $key => $value) {
            $menuPagesNames[$key] = $this->dashesToCamelCase($value);
        }
        return $menuPagesNames;
    }

    /**
     * @return int
     */
    protected function getMenuPageIdFromRoute(): int
    {
        switch (Route::currentRouteName()) {
            case $this->menuPageRoutes[Page::MAIN]:
                return Page::MAIN;
            case $this->menuPageRoutes[Page::ABOUT_US]:
                return Page::ABOUT_US;
            case $this->menuPageRoutes[Page::SERVICES]:
                return Page::SERVICES;
            case $this->menuPageRoutes[Page::OUR_WORK]:
                return Page::OUR_WORK;
            case $this->menuPageRoutes[Page::NEWS]:
                return Page::NEWS;
            case $this->menuPageRoutes[Page::CONTACTS]:
                return Page::CONTACTS;
            default:
                LogRec::error('Looks like seo is not set for '. Route::currentRouteName());
                return 0;
        }
    }
    #endregion
}
