<?php
/**
 * User: Andrey Naumoff
 * Date: 13-Jan-19
 * Time: 3:29 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\DataHelpers;


use App\Models\PageModels\PageType;

trait SliderSinglePages
{
    private $singlePagesForSlider = [
        [
            'name' => 'development',
            'page_type_id' => PageType::SINGLE,
        ],
        [
            'name' => 'security',
            'page_type_id' => PageType::SINGLE,
        ],
        [
            'name' => 'integration',
            'page_type_id' => PageType::SINGLE,
        ],
    ];

    private $singlePagesMetatags = [
        'development' => [
            'header_en' => 'Development',
            'title_en' => 'Development',
            'description_en' => '',
            'keywords_en' => '',
            'header_ru' => 'Разработка',
            'title_ru' => 'Разработка',
            'description_ru' => '',
            'keywords_ru' => '',
            'header_ua' => 'Розробка',
            'title_ua' => 'Розробка',
            'description_ua' => '',
            'keywords_ua' => '',
        ],
        'security' => [
            'header_en' => 'Security',
            'title_en' => 'Security',
            'description_en' => '',
            'keywords_en' => '',
            'header_ru' => 'Безопасность',
            'title_ru' => 'Безопасность',
            'description_ru' => '',
            'keywords_ru' => '',
            'header_ua' => 'Безпека',
            'title_ua' => 'Безпека',
            'description_ua' => '',
            'keywords_ua' => '',
        ],
        'integration' => [
            'header_en' => 'Integration',
            'title_en' => 'Integration',
            'description_en' => '',
            'keywords_en' => '',
            'header_ru' => 'Интеграция',
            'title_ru' => 'Интеграция',
            'description_ru' => '',
            'keywords_ru' => '',
            'header_ua' => 'Інтеграція',
            'title_ua' => 'Інтеграція',
            'description_ua' => '',
            'keywords_ua' => '',
        ]
    ];

    private $singlePagesRawHtmls = [
        'development' => [
            'name' => 'pageInput',
            'description' => 'raw html slider page input',
            'en' => '<div class="shell">            <div class="range range-70 range-sm-center range-lg-justify">                <div class="cell-sm-10 cell-md-8 cell-lg-8">                    <h4>DEVELOP YOUR COMPLEX DATA SYSTEM WITH OPEN SOURCE</h4>                    <p>                        OPEN SOURCE ENTERPRISE SOLUTIONS for Companies working with big volume dynamic data bases.                        All in one - from backend to frontend.                    </p><br>                    <h6>Example:</h6>                    <ul>                        <li>Virtual servers - 350</li>                        <li>System is divided in 3 vertical equal zones</li>                        <li>Files’ volume in object storage - 80 Tb. Increasing + 3,5 Tb/month</li>                        <li>Volume of NoSQL productive data base - 230 Gb</li>                        <li>200 000 system users</li>                        <li>Quantity of fixed sessions per day - 21 mln.</li>                        <li>Traffic per day: 442 Gb downloaded, 88 Gb uploaded</li>                    </ul>                    <p>                        SYSTEM OF GENERAL MONITORING, ALERT, LOGGING, RESERVATION and LOADS BALANCING guarantees Your safety and replaces usual SLAs of Vendors.                    </p><br>                    <h6>Example:</h6>                    <p> Monitoring system provides quick response thanks to ready-made scripts and settings, graphs, dashboards, as well as alerts, configured for triggers by system levels and services.                        Logging system ensures transparency of all changes in the system and the possibility of complete audit.                        Load balancing ensures functioning of systems by distributing large loads.                    </p>                    <h4>WHY OPEN SOURCE TECHNOLOGIES:</h4>                    <p>                        OPEN SOURCE is the safest software — a code is developed and audited by the largest number of specialists in the world.                    </p>                    <p>                        OPEN SOURCE is transparent, unlike vendor solutions, all changes are traceable.                    </p>                    <p>                        OPEN SOURCE is fast track to innovations in IT.                    </p>                    <p>                        Implementation of OPEN SOURCE solutions stimulates development of customer’s internal team, preventing degradation of Your admins which is the case when taking vendor solution.                    </p>                    <p>                        SUPPORT ON SITE — an in-house team/team on site better understands internal processes and needs, leading to less resources spent for development аnd resulting in to exactly the system Your business requires.                        Open documentation — normally in vendor solutions a product is 50% of the price, another 50% is implementation, training/learning and documentation.                        With open products a customer has a choice — to take or not to take professional support, unlike with vendor solutions, where paid support is normally obligatory.                        Experience tells, that even if You have SLA, but a problem is not obvious, its solution is sold as additional option or development. With open source alike situations are much more transparent.                        Price depends on complexity of necessary solutions and individual development needs, but in general Your economy may achieve 30-50% of alike vendor solution.                    </p>                </div>                <div class="cell-sm-10 cell-md-4">                    <div class="row grid-2">                        <div class="col-xs-12"><img src="/site/images/photo_2018-09-23_12-14-44.jpg" alt="" width="273" height="214"/>                        </div>                        <div class="col-xs-6"><img src="/site/images/about-1-273x214.jpg" alt="" width="273" height="214"/><img src="/site/images/about-2-273x214.jpg" alt="" width="273" height="214"/>                        </div>                        <div class="col-xs-6"><img src="/site/images/about-3-273x451.jpg" alt="" width="273" height="451"/>                        </div>                    </div>                </div>            </div>        </div>',
            'ru' => '',
            'ua' => '',
        ],
        'security' => [
            'name' => 'pageInput',
            'description' => 'raw html slider page input',
            'en' => '<div class="shell">            <div class="range range-70 range-sm-center range-lg-justify">                <div class="cell-sm-10 cell-md-8 cell-lg-8">                    <h4>MAKE YOUR SYSTEM SECURE, STABLE AND PCIDSS COMPLIANT.</h4>                    <p>                        We perform EXTERNAL & INTERNAL PENETRATION TESTS and complex scanning of your IT infrastructure to help in search for vulnerabilities.                        Based on such diagnostics we implement security policies, procedures and reports, compliant with PCI DSS.                        There are lots of IT life hacks to implement in any company to make IT SECURITY EASIER.                        STABILITY IS ALSO SECURITY                        Systems design and architecture is what makes them stable, we add loads balancing, automated backups and logging to fine-tune our architecture.                        We have a great track record of servers up and working as well as 24/7 support.                        An if there is an incident 15 minute reaction and less than 24h fixing even critical incidents is guaranteed.                    </p>                </div>                <div class="cell-sm-10 cell-md-4">                    <div class="row grid-2">                        <div class="col-xs-6"><img src="/site/images/about-1-273x214.jpg" alt="" width="273" height="214"/><img src="/site/images/about-2-273x214.jpg" alt="" width="273" height="214"/>                        </div>                        <div class="col-xs-6"><img src="/site/images/about-3-273x451.jpg" alt="" width="273" height="451"/>                        </div>                    </div>                </div>            </div>        </div>',
            'ru' => '',
            'ua' => '',
        ],
        'integration' => [
            'name' => 'pageInput',
            'description' => 'raw html slider page input',
            'en' => '<div class="shell">            <div class="range range-70 range-sm-center range-lg-justify">                <div class="cell-sm-10 cell-md-8 cell-lg-8">                    <h4>INTEGRATE YOUR TOOLS FROM ZERO OR INTO EXISTING INFRASTRUCTURE</h4>                    <p>                        Our biggest case is PROZORRO - the first company with large scale dynamic data base, that dared to move their CBD from AMAZON to Ukraine!                        For that purpose we had do build AMAZON-ALIKE infrastructure and functionality, based on available open source solutions, in local data centers.                        MK-CONSULTING team has been providing data transfer, transfer of system\'s modules, support and development for PROZORRO, with 200 000+ active system users, 300+ servers administration, development of modules from sandbox to deployment and production.                    </p>                </div>                <div class="cell-sm-10 cell-md-4">                    <div class="row grid-2">                        <div class="col-xs-6"><img src="/site/images/about-1-273x214.jpg" alt="" width="273" height="214"/><img src="/site/images/about-2-273x214.jpg" alt="" width="273" height="214"/>                        </div>                        <div class="col-xs-6"><img src="/site/images/about-3-273x451.jpg" alt="" width="273" height="451"/>                        </div>                    </div>                </div>            </div>        </div>',
            'ru' => '<div class="shell">            <div class="range range-70 range-sm-center range-lg-justify">                <div class="cell-sm-10 cell-md-8 cell-lg-8">                    <h4>ИНТЕГРИРУЙТЕ СВОИ ИНСТРУМЕНТЫ С НУЛЯ ИЛИ В СУЩЕСТВУЮЩУЮ ИНФРАСТРУКТУРУ</h4>                    <p>                        Наш самый большой кейс - это первая компания с крупномасштабной динамической базой данных, которая осмелилась перенести свои мощности из AWS на собственные мощности в Украине!                        Для этого мы создали инфраструктуру по функциональности похожую на AWS, основанную на доступных решениях с открытым исходным кодом, в локальных центрах обработки данных.                        Команда MK-CONSULTING обеспечивает передачу данных, передачу модулей системы, поддержку и разработку для Прозорро, с 200 000 + активных пользователей системы, администрирование 300 + серверов, разработку модулей от песочницы до развертывания на проде.                    </p>                </div>                <div class="cell-sm-10 cell-md-4">                    <div class="row grid-2">                        <div class="col-xs-6"><img src="/site/images/about-1-273x214.jpg" alt="" width="273" height="214"/><img src="/site/images/about-2-273x214.jpg" alt="" width="273" height="214"/>                        </div>                        <div class="col-xs-6"><img src="/site/images/about-3-273x451.jpg" alt="" width="273" height="451"/>                        </div>                    </div>                </div>            </div>        </div>    </section>',
            'ua' => '',
        ],
    ];
}
