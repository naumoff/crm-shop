<?php
/**
 * User: Andrey Naumoff
 * Date: 04-Jan-19
 * Time: 5:28 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\DataHelpers;

use App\Models\UserModels\Role;

trait UserRoles
{
    #region TRAIT PROPERTIES
    protected $roleNames = [
        Role::ADMIN => 'admin',
        Role::CONTENT_MANAGER => 'content-manager',
        Role::CONTENT_EDITOR => 'content-editor',
        Role::CUSTOMER => 'customer',
        Role::SUBSCRIBER => 'subscriber',
        Role::UNDEFINED => 'undefined' // guest, unverified, no roles assigned - virtual parameter
    ];
    #endregion

    /**
     * @return array
     */
    private function getRealRoles(): array
    {
        return array_except($this->roleNames, [Role::UNDEFINED]);
    }
}
