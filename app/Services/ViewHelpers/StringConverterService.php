<?php
/**
 * User: Andrey Naumoff
 * Date: 08-Feb-19
 * Time: 12:02 AM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\ViewHelpers;


class StringConverterService
{
    /**
     * @param string $string
     * @return string
     */
    public function convertDashesToWhiteSpace(string $string): string
    {
        return str_replace('-', ' ', $string);
    }
}
