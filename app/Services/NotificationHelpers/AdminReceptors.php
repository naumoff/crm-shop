<?php
/**
 * User: Andrey Naumoff
 * Date: 28-Jan-19
 * Time: 10:19 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\NotificationHelpers;

use App\Models\UserModels\Role;
use Illuminate\Support\Collection;

trait AdminReceptors
{
    /**
     * @return Collection|null
     */
    private function getReceptors(): ?Collection
    {
        $admins = Role::admin()->first()->users->where('approved', '=', 1);
        return $admins;
    }
}
