<?php
/**
 * User: Andrey Naumoff
 * Date: 23-Feb-19
 * Time: 9:43 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\RequestFilters;

use Illuminate\Http\Request;

abstract class ArrayFilters
{
    #region PROPERTIES
    /** @var Request $request */
    protected $request;

    /** @var array $filters */
    protected $filters = [];
    #endregion

    #region MAIN METHODS
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    protected function getFilters(): array
    {
        return $this->request->only($this->filters);
    }

    /**
     * @param array $array
     * @return array
     */
    abstract public function apply(array &$array): void;

    /**
     * set get param names expected from query
     */
    abstract protected function setFilterKeys(): void;
    #endregion
}
