<?php
/**
 * User: Andrey Naumoff
 * Date: 23-Feb-19
 * Time: 9:51 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\RequestFilters;

use Illuminate\Support\Str;
use Illuminate\Http\Request;

final class UserFilters extends ArrayFilters
{
    #region MAIN METHODS
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->setFilterKeys();
    }

    /**
     * @param array $array
     */
    public function apply(array &$users): void
    {
        foreach ($this->getFilters() as $filter => $value) {
            if (!empty($value)) {
                $methodName = $filter.'Filter';
                if (method_exists($this, $methodName)) {
                    $users = $this->$methodName($users, $value);
                }
            }
        }
    }
    #endregion

    #region SERVICE METHODS
    /**
     * put get params names here
     */
    protected function setFilterKeys(): void
    {
        $this->filters = ['email', 'name'];
    }

    /**
     * method loaded dynamically from apply public method
     * @param array $users
     * @param string $value
     * @return array
     */
    private function nameFilter(array &$users, string $value): array
    {
        return array_filter($users, function ($user) use ($value) {
            return Str::contains(strtolower($user->name), strtolower($value));
        });
    }

    /**
     * method loaded dynamically from apply public method
     * @param array $users
     * @param string $value
     * @return array
     */
    private function emailFilter(array &$users, string $value): array
    {
        return array_filter($users, function ($user) use ($value) {
            return Str::contains(strtolower($user->email), strtolower($value));
        });
    }
    #endregion
}
