<?php
/**
 * User: Andrey Naumoff
 * Date: 17-Dec-18
 * Time: 7:42 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\DBCache;

use App\Contracts\DBCacheContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;
use stdClass;

/**
 * Class DBCacheRedisService
 * @package App\Services\DBCache
 */
final class DBCacheRedisService implements DBCacheContract
{
    #region PROPERTIES
    /** @var \Redis $redis */
    private $redis;

    /** @var string $connection */
    private $connection = 'default';
    #endregion

    #region MAIN METHODS
    public function __construct()
    {
        $this->redis = Redis::connection($this->connection);
    }

    /**
     * @param Model $model
     * @param string $key
     * @param string|null $customModelKeyField (any name different from 'id')
     */
    public function saveModel(Model $model, string $key, string $customModelKeyField = null): void
    {
        if (empty($customModelKeyField)) {
            $this->redis->hset($key, $model->id, $model->toJson());
        } else {
            $this->redis->hset($key, $model->$customModelKeyField, $model->toJson());
        }
    }

    /**
     * @param Model $model
     * @param string $key
     * @param string|null $customModelKeyField (any name different from 'id')
     */
    public function deleteModel(Model $model, string $key, string $customModelKeyField = null): void
    {
        if (empty($customModelKeyField)) {
            $this->redis->hdel($key, $model->id);
        } else {
            $this->redis->hdel($key, $model->$customModelKeyField);
        }
    }

    /**
     * @param string $key
     * @return array
     */
    public function getModelsByKey(string $key): array
    {
        $jsonModels = $this->redis->hgetall($key);
        sort($jsonModels);
        $arrayModels = array_map(function ($hash) {
            return json_decode($hash);
        },
            $jsonModels);
        return $arrayModels;
    }

    /**
     * @param string $key
     * @param int|string $modelKeyValue
     * @return stdClass
     */
    public function getModelByKeyAndId(string $key, $modelKeyValue): stdClass
    {
        $jsonModel = $this->redis->hget($key, $modelKeyValue);
        return json_decode($jsonModel);
    }
    #endregion
}
