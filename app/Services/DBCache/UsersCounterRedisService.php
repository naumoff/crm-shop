<?php
/**
 * User: Andrey Naumoff
 * Date: 07-Feb-19
 * Time: 11:09 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\DBCache;

use App\Contracts\ModelObserverContract;
use App\Contracts\UsersCounterContract;
use App\Services\DataHelpers\UserRoles;
use Redis;

class UsersCounterRedisService implements UsersCounterContract
{
    use UserRoles;

    #region PROPERTIES
    /** @var \Redis $redis */
    private $redis;

    /** @var string $connection */
    private $connection = 'default';
    #endregion

    #region MAIN METHODS
    public function __construct()
    {
        $this->redis = Redis::connection($this->connection);
    }

    /**
     * @return int
     */
    public function countAll(): int
    {
        return $this->redis->command('hlen', [ModelObserverContract::USER_ALL]);
    }

    /**
     * @return array
     */
    public function countRoles(): array
    {
        $result = [];
        foreach ($this->getRealRoles() as $roleName) {
            $result[$roleName] = $this->redis->command('hlen', ['user:' .$roleName]);
        }
        return $result;
    }

    /**
     * @return int
     */
    public function countNotApproved(): int
    {
        return $this->redis->command('hlen', [ModelObserverContract::USER_NOT_APPROVED]);
    }

    /**
     * @return int
     */
    public function countWithoutRole(): int
    {
        return $this->redis->command('hlen', [ModelObserverContract::USER_WITHOUT_ROLE]);
    }

    /**
     * @return int
     */
    public function countUnverified(): int
    {
        return $this->redis->command('hlen', [ModelObserverContract::USER_NOT_VERIFIED]);
    }

    /**
     * @return int
     */
    public function countDeleted(): int
    {
        return $this->redis->command('hlen', [ModelObserverContract::USER_DELETED]);
    }
    #endregion
}
