<?php
/**
 * User: Andrey Naumoff
 * Date: 01-Jan-19
 * Time: 2:51 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\Translation;

use App\Contracts\LocaleContract;

/**
 * Trait LocaleValidator
 * @package App\Services\TranslationHelpers
 */
trait LocaleValidator
{
    /**
     * @param string $locale
     * @return string
     */
    protected function validateAndGetLocale(string $locale): string
    {
        if (!in_array($locale, LocaleContract::AVAILABLE_LOCALES)) {
            $locale = LocaleContract::AVAILABLE_LOCALES['primary'];
        }
        return $locale;
    }
}
