<?php
/**
 * User: Andrey Naumoff
 * Date: 14-Feb-19
 * Time: 11:02 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Contracts;

interface MustVerifyEmailInvitationContract
{
    /**
     * @param string $temporaryPassword
     */
    public function sendEmailVerificationInvitation(string $temporaryPassword): void;
}
