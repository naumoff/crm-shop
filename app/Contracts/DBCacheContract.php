<?php
/**
 * User: Andrey Naumoff
 * Date: 17-Dec-18
 * Time: 7:41 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Contracts;

use Illuminate\Database\Eloquent\Model;
use stdClass;

interface DBCacheContract
{
    public function saveModel(Model $model, string $key, string $customModelKeyField = null): void;

    public function deleteModel(Model $model, string  $key, string $customModelKeyField = null): void;

    public function getModelsByKey(string $key): array;

    /**
     * @param string $key
     * @param int|string $modelKeyValue
     * @return stdClass
     */
    public function getModelByKeyAndId(string $key, $modelKeyValue): stdClass;
}
