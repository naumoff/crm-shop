<?php
/**
 * User: Andrey Naumoff
 * Date: 14-Jan-19
 * Time: 10:43 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Contracts;

use stdClass;

interface PageViewContract
{
    /**
     * @param stdClass $cachedPageModel
     * @param string $locale
     * @return array
     */
    public function fetchVariablesArray(stdClass $cachedPageModel, string $locale): array;
}
