<?php
/**
 * User: Andrey Naumoff
 * Date: 26-Jan-19
 * Time: 10:32 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Contracts;

interface SlackWebHookContract
{
    /**
     * @return string
     */
    public function getSlackWebHook(): string;
}
