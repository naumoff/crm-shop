<?php
/**
 * User: Andrey Naumoff
 * Date: 07-Feb-19
 * Time: 11:10 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Contracts;

interface UsersCounterContract
{
    /**
     * @return int
     */
    public function countAll(): int;

    /**
     * @return array
     */
    public function countRoles(): array;

    /**
     * @return int
     */
    public function countNotApproved(): int;

    /**
     * @return int
     */
    public function countWithoutRole(): int;

    /**
     * @return int
     */
    public function countUnverified(): int;

    /**
     * @return int
     */
    public function countDeleted(): int;
}
