<?php
/**
 * User: Andrey Naumoff
 * Date: 27-Jan-19
 * Time: 8:10 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Facades;

use App\Contracts\LogRecorderContract;
use Illuminate\Support\Facades\Facade;

class LogRecorderFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return LogRecorderContract::class;
    }
}
