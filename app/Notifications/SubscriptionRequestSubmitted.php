<?php

namespace App\Notifications;

use App\Contracts\SlackWebHookContract;
use App\Models\RequestModels\SubscribeRequest;
use App\Models\UserModels\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SubscriptionRequestSubmitted extends Notification implements ShouldQueue, SlackWebHookContract
{
    use Queueable;

    #region PROPERTIES
    /** @var SubscribeRequest $subscribeRequest */
    private $subscribeRequest;

    /** @var int $messageCounter */
    private $messageCounter = 0;
    #endregion

    #region MAIN METHODS

    /**
     * SubscriptionRequestSubmitted constructor.
     * @param SubscribeRequest $subscribeRequest
     */
    public function __construct(SubscribeRequest $subscribeRequest)
    {
        $this->subscribeRequest = $subscribeRequest;
    }

    /**
     * Get the notification's delivery channels.
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        $this->messageCounter += 1;
        if ($this->messageCounter === 1) {
            return ['mail', 'database', 'slack']; //send notification to slack and database only one time
        } else {
            return ['mail'];
        }
    }

    /**
     * Get the mail representation of the notification.
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        list($user, $email, $preferredLocale) = $this->getSubscriptionDetails();
        return (new MailMessage)
            ->subject('Subscription Request Attempt Submitted')
            ->markdown(
                'mail.subscription.request.submitted',
                [
                    'user' => $user,
                    'email' => $email,
                    'locale' => $preferredLocale
                ]
            );
    }

    /**
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable): array
    {
        list($user, $email, $preferredLocale) = $this->getSubscriptionDetails();
        return [
            'user' => $user,
            'email' => $email,
            'preferredLocale' => $preferredLocale,
        ];
    }

    /**
     * @param $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable): SlackMessage
    {
        list($user, $email, $preferredLocale) = $this->getSubscriptionDetails(true);
        return (new SlackMessage)
            ->from($user, ':ghost:')
            ->content('Subscription request submitted:')
            ->attachment(function ($attachment) use ($email, $preferredLocale) {
                $attachment->title('Email: ' . $email)
                    ->content('Preferred Locale: ' . $preferredLocale);
            });
    }

    /**
     * This method called in User model User::routeNotificationForSlack()
     * and provides ability to send message to different web hooks depending on each Notification class
     * @return string
     */
    public function getSlackWebHook(): string
    {
        return config('slack.web_hooks.subscription');
    }

    /**
     * Get the array representation of the notification.
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param bool $shortUserName
     * @return array
     */
    private function getSubscriptionDetails($shortUserName = false): array
    {
        if ($this->subscribeRequest->user_id === null) {
            $user = 'Undefined user';
        } else {
            /** @var User $requestedUser */
            $requestedUser = User::find($this->subscribeRequest->user_id);
            if ($shortUserName === false) {
                $user = ucfirst($requestedUser->name) . ', registered with email ' . $requestedUser->email . ', ';
            } else {
                $user = ucfirst($requestedUser->name);
            }
        }
        $email = $this->subscribeRequest->email;
        $preferredLocale = $this->subscribeRequest->locale;

        return [$user, $email, $preferredLocale];
    }
    #endregion
}
