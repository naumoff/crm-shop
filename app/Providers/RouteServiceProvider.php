<?php

namespace App\Providers;

use App\Contracts\DBCacheContract;
use App\Contracts\ModelObserverContract;
use App\Models\UserModels\User;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    #region PROPERTIES
    /**
     * This namespace is applied to your controller routes.
     * In addition, it is set as the URL generator's root namespace.
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /** @var DBCacheContract $dbCacheService */
    private $dbCacheService;
    #endregion

    #region MAIN METHODS
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->setDbCacheService();

        $this->mapApiRoutes();

        $this->mapAuthRoutes();

        $this->mapAdminRoutes();

        $this->mapCMSRoutes();

        $this->mapWebRoutes();
    }
    #endregion

    #region SERVICE METHODS
    /**
     * Define routes in administration panel.
     */
    protected function mapAdminRoutes(): void
    {
        Route::bind('userStatus', function ($userStatus) {
            return $this->dbCacheService->getModelsByKey('user:'.$userStatus);
        });

        Route::bind('userWithTrashed', function (int $userWithTrashedId) {
            return User::withTrashed()->find($userWithTrashedId);
        });

        Route::bind('deletedUser', function (int $deletedUser) {
            return User::onlyTrashed()->find($deletedUser);
        });

        Route::middleware(['web', 'verified'])
            ->namespace($this->namespace.'\Dashboard\Admin')
            ->group(base_path('routes/dashboards/admin.php'));
    }

    /**
     * Define routes in cms panel.
     */
    protected function mapCMSRoutes(): void
    {
        Route::bind('pageType', function ($id) {
            $key = ModelObserverContract::PAGE_TYPE_KEY;
            return $this->dbCacheService->getModelByKeyAndId($key, $id);
        });
        Route::middleware(['web', 'verified'])
            ->namespace($this->namespace.'\Dashboard\CMS')
            ->group(base_path('routes/dashboards/cms.php'));
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes(): void
    {
        Route::bind('singlePage', function ($id) {
            $key = ModelObserverContract::PAGE_SINGLE_KEY;
            return $this->dbCacheService->getModelByKeyAndId($key, $id);
        });

        Route::middleware('web')
             ->namespace($this->namespace.'\Front')
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define authorization routes
     */
    protected function mapAuthRoutes(): void
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/auth.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes(): void
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    private function setDbCacheService(): void
    {
        if (empty($this->dbCacheService)) {
            $this->dbCacheService = app(DBCacheContract::class);
        }
    }
    #endregion
}
