<?php

namespace App\Providers;

use App\Contracts\SitePageAccessContract;
use App\Models\UserModels\User;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    #region PROPERTIES
    /** @var null|SitePageAccessContract $sitePageAccessService */
    private $sitePageAccessService;

    /** @var array $pagesRights */
    private $pageRights = [
        SitePageAccessContract::VIEW_MENU_PAGE,
    ];

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\UserModels' => 'App\Policies\ModelPolicy',
    ];

    #endregion

    #region MAIN METHODS
    public function __construct($app)
    {
        parent::__construct($app);
    }

    /**
     * @param SitePageAccessContract $sitePageAccess
     */
    public function boot(SitePageAccessContract $sitePageAccess): void
    {
        $this->registerPolicies();

        $this->sitePageAccessService = $sitePageAccess;

        foreach ($this->pageRights as $right) {
            /** pages rights for guests and users */
            Gate::define($right, function (?User $user, int $pageId) use ($right) {
                return $this->sitePageAccessService->$right($user, $pageId);
            });
        }
    }
    #endregion
}
