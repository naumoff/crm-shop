<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ContactRequestPost
 * @todo complete validation
 * @package App\Http\Requests
 */
class ContactRequestPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact_name' => 'required|alpha',
            'phone' => 'required',
            'email' => 'required',
            'message' => 'required',
        ];
    }
}
