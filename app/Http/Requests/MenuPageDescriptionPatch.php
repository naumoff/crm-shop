<?php

namespace App\Http\Requests;

use App\Contracts\LocaleContract;
use App\Models\PageModels\PageType;
use Illuminate\Foundation\Http\FormRequest;

class MenuPageDescriptionPatch extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validators = [];
        foreach (LocaleContract::AVAILABLE_LOCALES as $key => $locale) {
            if ($key === 'primary') {
                $validators['description_'.$locale] = 'required|max:160|string';
            } else {
                $validators['description_'.$locale] = 'max:160|string|nullable';
            }
        }
        $validators['page_id'] = 'required|integer|exists:pages,id';
        $validators['page_type_id'] = 'required|integer|exists:pages,page_type_id|size:'.PageType::MENU;
        return $validators;
    }
}
