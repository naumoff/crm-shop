<?php

namespace App\Http\Controllers\Front;

use App\Events\ContactRequestSubmitted;
use App\Events\SubscriptionRequestSubmitted;
use App\Http\Controllers\Controller;
use App\Models\RequestModels\ContactRequest;
use App\Models\RequestModels\SubscribeRequest;
use App\Services\Translation\LocaleValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use LogRec;

class RequestController extends Controller
{
    use LocaleValidator;

    #region MAIN METHODS
    /**
     * temporary solution before knowing how js works in bought template
     * @todo remove ugly code
     * @param Request $request
     * @param string $locale
     */
    public function storeContactRequest(Request $request, string $locale): void
    {
        $validator = Validator::make(
            $request->all(),
            [
                'contact_name' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'message' => 'required',
            ]
        );

        if ($validator->fails()) {
            echo 'MF255';
            return;
        }

        try {
            $contactRequest = new ContactRequest();
            $contactRequest->user_id = (empty(\Auth::user()))? null : \Auth::user()->id;
            $contactRequest->contact_name = $request['contact_name'];
            $contactRequest->phone = $request['phone'];
            $contactRequest->message = $request['message'];
            $contactRequest->email = $request['email'];
            $contactRequest->locale = $this->validateAndGetLocale($locale);
            $contactRequest->save();
            echo 'MF000';
            event(new ContactRequestSubmitted($contactRequest));
        } catch (\Throwable $throwable) {
            LogRec::error($throwable->getMessage());
            echo 'MF255';
        }
    }

    /**
     * temporary solution before knowing how js works in bought template
     * @todo remove ugly code
     * @param Request $request
     * @param string $locale
     */
    public function storeSubscriptionRequest(Request $request, string $locale): void
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|unique:subscribe_requests,email',
            ]
        );

        if ($validator->fails()) {
            echo 'MF255';
            return;
        }

        try {
            $subscribeRequest = new SubscribeRequest();
            $subscribeRequest->user_id = (empty(\Auth::user()))? null : \Auth::user()->id;
            $subscribeRequest->email = $request['email'];
            $subscribeRequest->locale = $this->validateAndGetLocale($locale);
            $subscribeRequest->save();
            echo 'MF000';
            event(new SubscriptionRequestSubmitted($subscribeRequest));
        } catch (\Throwable $throwable) {
            LogRec::error($throwable->getMessage());
            echo 'MF255';
        }
    }
    #endregion
}
