<?php

namespace App\Http\Controllers\Front;

use App\Contracts\LocaleContract;
use App\Contracts\PageViewContract;
use Illuminate\Http\Request;
use stdClass;

class PageController extends FrontController
{
    #region PROPERTIES
    /** @var PageViewContract $pageViewService */
    private $pageViewService;
    #endregion

    #region MAIN METHODS
    /**
     * PageController constructor.
     * @param LocaleContract $localeService
     * @param PageViewContract $pageViewService
     */
    public function __construct(LocaleContract $localeService, PageViewContract $pageViewService)
    {
        parent::__construct($localeService);
        $this->pageViewService = $pageViewService;
    }

    /**
     * @param null|string $locale
     * @param stdClass $cachedModel
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(?string $locale, stdClass $cachedModel)
    {
        return view($cachedModel->view_path, $this->pageViewService->fetchVariablesArray($cachedModel, $this->locale));
    }
    #endregion
}
