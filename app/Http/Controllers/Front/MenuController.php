<?php

namespace App\Http\Controllers\Front;

use App\Contracts\SitePageAccessContract;
use App\Models\PageModels\Page;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;

class MenuController extends FrontController
{
    #region MAIN METHODS
    public function index()
    {
        $view = view('site.controllers.menu.index');
        return $this->passTroughViewRight(Page::MAIN, $view);
    }

    public function aboutUs()
    {
        $view = view('site.controllers.menu.about_us');
        return $this->passTroughViewRight(Page::ABOUT_US, $view) ;
    }

    public function services()
    {
        $view = view('site.controllers.menu.services');
        return $this->passTroughViewRight(Page::SERVICES, $view);
    }

    public function ourWork()
    {
        $view = view('site.controllers.menu.our_work');
        return $this->passTroughViewRight(Page::OUR_WORK, $view);
    }

    public function news()
    {
        $view = view('site.controllers.menu.news');
        return $this->passTroughViewRight(Page::NEWS, $view);
    }

    public function contacts()
    {
        $view = view('site.controllers.menu.contacts');
        return $this->passTroughViewRight(Page::CONTACTS, $view);
    }

    public function notFoundPage()
    {
        return view('site.controllers.menu.not_found_page');
    }
    #endregion

    #region SERVICE METHODS

    /**
     * @param int $pageId
     * @param View $view
     * @return RedirectResponse|View
     */
    private function passTroughViewRight(int $pageId, View $view)
    {
        if (Gate::allows(SitePageAccessContract::VIEW_MENU_PAGE, $pageId)) {
            return $view;
        } else {
            return redirect()->route('not-found', ['locale' => $this->locale]);
        }
    }
    #endregion
}
