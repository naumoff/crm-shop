<?php
/**
 * User: Andrey Naumoff
 * Date: 25-Nov-18
 * Time: 10:23 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Http\Controllers\Dashboard\CMS\Manager;

use App\Contracts\ModelObserverContract;
use App\Http\Requests\MenuPageDescriptionPatch;
use App\Http\Requests\MenuPageHeaderPatch;
use App\Http\Requests\MenuPageKeywordsPatch;
use App\Http\Requests\MenuPageTitlePatch;
use App\Models\PageModels\MetadataField;
use App\Models\PageModels\Page;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use LogRec;

/**
 * Class MenuPageSeoController
 * @package App\Http\Controllers\Dashboard\CMS
 */
class MenuPageSeoController extends ManagerController
{
    /**
     * @param int $pageId
     * @return Factory|View
     */
    public function seoEdit(int $pageId)
    {
        $page = $this->dbCacheService->getModelByKeyAndId(ModelObserverContract::PAGE_MENU_KEY, $pageId);
        return view('dashboards.cms.manager.controllers.menu_page_seo.seo_edit')
            ->with(['pageModel' => $page]);
    }

    /**
     * @param MenuPageTitlePatch $request
     * @param Page $page
     * @return RedirectResponse
     */
    public function menuPageMetadataTitleUpdate(MenuPageTitlePatch $request, Page $page): RedirectResponse
    {
        $field = 'title';
        return $this->handleMetadataPatch($request, $page, $field);
    }

    /**
     * @param MenuPageDescriptionPatch $request
     * @param Page $page
     * @return RedirectResponse
     */
    public function menuPageMetadataDescriptionUpdate(MenuPageDescriptionPatch $request, Page $page): RedirectResponse
    {
        $field = 'description';
        return $this->handleMetadataPatch($request, $page, $field);
    }

    /**
     * @param MenuPageKeywordsPatch $request
     * @param Page $page
     * @return RedirectResponse
     */
    public function menuPageMetadataKeywordsUpdate(MenuPageKeywordsPatch $request, Page $page): RedirectResponse
    {
        $field = 'keywords';
        return $this->handleMetadataPatch($request, $page, $field);
    }

    /**
     * @param MenuPageHeaderPatch $request
     * @param Page $page
     * @return RedirectResponse
     */
    public function menuPageMetadataHeaderUpdate(MenuPageHeaderPatch $request, Page $page): RedirectResponse
    {
        $field = 'header';
        return $this->handleMetadataPatch($request, $page, $field);
    }
    #endregion

    #region SERVICE METHODS

    /**
     * @param Request $request
     * @param Page $page
     * @param string $field
     * @return RedirectResponse
     */
    private function handleMetadataPatch(Request $request, Page $page, string $field): RedirectResponse
    {
        $validatedRequest = $request->validated();
        if ($page->id === (integer)$validatedRequest['page_id'] &&
            $page->page_type_id === (integer)$validatedRequest['page_type_id']) {
            $titleUpdated = $this->updatePageMetadata($page, $validatedRequest, $field);
            if ($titleUpdated) {
                session()->flash('message', ucfirst($field) . ' tagged to page successfully!');
            }
            return redirect()->back();
        } else {
            session()->flash('error', 'Something goes wrong, '. $field .' did not updated!');
            LogRec::error("user with email ". Auth::user()->email ." is trying to cheat page '. $field .' form!");
            return redirect()->back();
        }
    }

    /**
     * @param Page $page
     * @param array $validatedRequest
     * @param string $field
     * @return bool
     */
    private function updatePageMetadata(Page $page, array $validatedRequest, string $field): bool
    {
        try {
            $metadataField = MetadataField::where('page_id', $page->id)->first();
            if ($metadataField === null) {
                $metadataField = new MetadataField();
            }
            unset($validatedRequest['page_type_id']);
            foreach ($validatedRequest as $column => $value) {
                $metadataField->$column = $value;
            }
            $page->metadataField()->save($metadataField);
            // page model touched in order to load page metadata to redis
            $page->touch();
            return true;
        } catch (\Throwable $throwable) {
            session()->flash('error', 'Something goes wrong, '. $field .' did not updated!');
            LogRec::error('Something goes wrong, '. $field .' did not updated!');
            LogRec::error($throwable->getMessage());
            return false;
        }
    }
    #endregion
}
