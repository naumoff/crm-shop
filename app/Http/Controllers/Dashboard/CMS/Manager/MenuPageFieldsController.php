<?php

namespace App\Http\Controllers\Dashboard\CMS\Manager;

use App\Contracts\DBCacheContract;
use App\Contracts\ModelObserverContract;
use App\Http\Controllers\Controller;
use App\Models\PageModels\BooleanField;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class MenuPageFieldsController extends ManagerController
{
    /**
     * @param int $pageId
     * @return Factory|View
     */
    public function fieldsEdit(int $pageId)
    {
        $page = $this->dbCacheService->getModelByKeyAndId(ModelObserverContract::PAGE_MENU_KEY, $pageId);
        return view('dashboards.cms.manager.controllers.menu_page_fields.fields_edit')
            ->with(['pageModel' => $page]);
    }

    /**
     * @param BooleanField $booleanField
     */
    public function menuPageFieldsBooleansUpdate(BooleanField $booleanField): void
    {
        $booleanField->value = ($booleanField->value)? 0:1;
        $booleanField->save();
        $booleanField->page->touch();
    }
    #endregion
}
