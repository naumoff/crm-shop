<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Contracts\ArrayPaginationContract;
use App\Events\NewUserInvited;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNewUserPost;
use App\Http\Requests\UpdateUserPatch;
use App\Models\UserModels\User;
use App\Services\DataHelpers\UserRoles;
use App\Services\RequestFilters\UserFilters;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AdminController extends Controller
{
    use UserRoles;

    #region PROPERTIES
    /** @var ArrayPaginationContract $arrayPaginator */
    private $arrayPaginator;
    #endregion

    #region MAIN METHODS
    /**
     * AdminController constructor.
     * @param ArrayPaginationContract $arrayPaginator
     */
    public function __construct(ArrayPaginationContract $arrayPaginator)
    {
        $this->arrayPaginator = $arrayPaginator;
    }

    /**
     * @return View
     */
    public function welcome(): View
    {
        return view('dashboards.admin.controllers.admin.welcome');
    }

    /**
     * @param array $users
     * @param UserFilters $filterService
     * @return View
     */
    public function users(array $users, UserFilters $filterService): View
    {
        $status = request()->segment(3);
        $filterService->apply($users);
        $paginatedUsers = $this->arrayPaginator->paginateArray(request(), $users, 15);
        return view('dashboards.admin.controllers.admin.users', ['users' => $paginatedUsers, 'status' => $status]);
    }

    /**
     * @param UpdateUserPatch $request
     * @param User $user
     */
    public function userUpdate(UpdateUserPatch $request, User $userWithTrashed)
    {
        $validatedInput = $request->validated();
        if (empty($validatedInput['userRoles'])) {
            $validatedInput['userRoles'] = [];
        }
        if ((int)$validatedInput['userId'] === $userWithTrashed->id) {
            $userWithTrashed->name = $validatedInput['userName'];
            $userWithTrashed->email = $validatedInput['userEmail'];
            $userWithTrashed->phone = $validatedInput['userPhone'];
            $userWithTrashed->address = $validatedInput['userAddress'];
            $userWithTrashed->location = $validatedInput['userLocation'];
            $userWithTrashed->approved = json_decode($validatedInput['userApproved']);
            $userWithTrashed->rolesMassUpdate($validatedInput['userRoles']);
            $userWithTrashed->save();
            session()->flash('message', 'User updated successfully');
            echo json_encode(['status' => 200]);
        }
    }

    /**
     * @param User $user
     */
    public function userDestroy(User $user): void
    {
        try {
            $user->delete();
            echo $user->id;
        } catch (\Throwable $throwable) {
            LogRec($throwable->getMessage());
            echo $throwable->getMessage();
        }
    }

    /**
     * @param User $user
     */
    public function userRestore(User $user): void
    {
        $user->restore();
        $user->touch();
        echo $user->id;
    }

    /**
     * @return View
     */
    public function userInvite(): View
    {
        return view('dashboards.admin.controllers.admin.user_invite');
    }

    /**
     * @param StoreNewUserPost $request
     * @return RedirectResponse
     */
    public function userCreate(StoreNewUserPost $request): RedirectResponse
    {
        $validatedInput = $request->validated();

        $newUser = new User();
        $newUser->name = $validatedInput['userName'];
        $newUser->email = $validatedInput['userEmail'];
        $newUser->approved = 1;
        $temporaryPassword = uniqid();
        $newUser->password = bcrypt($temporaryPassword);
        $newUser->save();
        foreach ($validatedInput as $column => $value) {
            if (!empty($this->getRealRoles()[$value]) && $this->getRealRoles()[$value] === $column) {
                $newUser->attachRole($value);
            }
        }
        event(new NewUserInvited($newUser, $temporaryPassword));
        session()->flash('message', "User with email {$newUser->email} successfully invited!");
        return redirect()->back();
    }
    #endregion
}
