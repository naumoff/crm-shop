<?php
/**
 * User: Andrey Naumoff
 * Date: 31-Dec-18
 * Time: 7:11 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Http\ViewComposers;

use App;
use App\Contracts\PageFieldsContract;
use App\Services\DataHelpers\MenuPageRoutes;
use Illuminate\Support\Facades\Route;
use Illuminate\View\View;

/**
 * Class MenuPageFieldsComposer
 * @package App\Http\ViewComposers
 */
class MenuPageFieldsComposer
{
    use MenuPageRoutes;

    #region PROPERTIES
    /** @var PageFieldsContract $pageFieldsService */
    private $pageFieldsService;

    /** @var string $locale */
    private $locale;
    #endregion

    #region MAIN METHODS
    /**
     * MenuPageFieldsComposer constructor.
     * @param PageFieldsContract $pageFieldsService
     */
    public function __construct(PageFieldsContract $pageFieldsService)
    {
        $this->pageFieldsService = $pageFieldsService;
        $this->locale = App::getLocale();
    }

    /**
     * @param View $view
     */
    public function compose(View $view): void
    {
        if (in_array(Route::currentRouteName(), $this->menuPageRoutes)) {
            $view->with('fields', $this->pageFieldsService->getMenuPageFields($this->locale));
        }
    }
    #endregion
}
