<?php
/**
 * User: Andrey Naumoff
 * Date: 17-Dec-18
 * Time: 10:35 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Http\ViewComposers;

use Request;
use Illuminate\View\View;

class ViewLinksComposer
{
    #region MAIN METHODS
    public function compose(View $view)
    {
        $view->with('asidePath', $this->getAsideViewPath());
        $view->with('headerNavItemPath', $this->getHeaderNavItemPath());
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @return null|string
     */
    private function getAsideViewPath(): ?string
    {
        if (Request::segment(1) === 'admin') {
            return 'dashboards.admin.inclusions.aside';
        } elseif (Request::segment(1) === 'cms-manager') {
            return 'dashboards.cms.manager.inclusions.aside_manager';
        } elseif (Request::segment(1) === 'cms-editor') {
            return 'cms editor';
        } else {
            return null;
        }
    }

    /**
     * @return null|string
     */
    private function getHeaderNavItemPath(): ?string
    {
        if (Request::segment(1) == 'admin') {
            return 'dashboards.admin.inclusions.nav_item';
        } elseif (Request::segment(1) == 'cms-manager') {
            return 'dashboards.cms.manager.inclusions.nav_item_manager';
        } elseif (Request::segment(1) == 'cms-editor') {
            return 'cms editor';
        } else {
            return null;
        }
    }
    #endregion
}
