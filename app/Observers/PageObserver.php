<?php

namespace App\Observers;

use App\Contracts\DBCacheContract;
use App\Contracts\ModelObserverContract;
use App\Models\PageModels\Page;
use App\Models\PageModels\PageType;
use Illuminate\Database\Eloquent\Model;

class PageObserver implements ModelObserverContract
{
    #region PROPERTIES
    /** @var DBCacheContract $cacheRecorder */
    private $dbCacheService;
    #endregion

    #region MAIN METHODS
    /**
     * PageObserver constructor.
     * @param DBCacheContract $dbCacheService
     */
    public function __construct(DBCacheContract $dbCacheService)
    {
        $this->dbCacheService = $dbCacheService;
    }

    /**
     * @param Model $page
     */
    public function saved(Model $page): void
    {
        if ($page->page_type_id === PageType::MENU) {
            $this->loadPageRelations($page);
            $this->savePage($page, self::PAGE_MENU_KEY);
        } elseif ($page->page_type_id === PageType::SINGLE) {
            $this->loadPageRelations($page);
            $this->savePage($page, self::PAGE_SINGLE_KEY, 'name');
        }
    }

    /**
     * @param Model $page
     */
    public function deleted(Model $page):void
    {
        if ($page->page_type_id === PageType::MENU) {
            $this->deletePage($page, self::PAGE_MENU_KEY);
        } elseif ($page->page_type_id === PageType::SINGLE) {
            $this->deletePage($page, self::PAGE_SINGLE_KEY, 'name');
        }
    }

    public function refreshed(): void
    {
        // TODO: Implement refreshed() method. Queue job is required here
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param Page $page
     */
    private function loadPageRelations(Page $page): void
    {
        $page->load('pageAccessGate');
        $page->load('metadataField');
        $page->load('booleanFields');
        $page->load('textFields');
        $page->load('longTextFields');
        $page->load('rawHtmlFields');
    }

    /**
     * @param Page $page
     * @param string $pageKey
     * @param string|null $customModelKeyField
     */
    private function savePage(Page $page, string $pageKey, string $customModelKeyField = null): void
    {
        $this->dbCacheService->saveModel($page, $pageKey, $customModelKeyField);
        $page->pageType->touch();
    }

    /**
     * @param Page $page
     * @param string $pageKey
     * @param string|null $customModelKeyField
     */
    private function deletePage(Page $page, string $pageKey, string $customModelKeyField = null): void
    {
        $this->dbCacheService->deleteModel($page, $pageKey, $customModelKeyField);
        $page->pageType->touch();
    }
    #endregion
}
