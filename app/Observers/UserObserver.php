<?php
/**
 * User: Andrey Naumoff
 * Date: 04-Feb-19
 * Time: 9:33 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Observers;

use App\Contracts\DBCacheContract;
use App\Contracts\ModelObserverContract;
use App\Services\DataHelpers\UserRoles;
use Illuminate\Database\Eloquent\Model;
use App\Models\UserModels\User;

class UserObserver implements ModelObserverContract
{
    use UserRoles;

    #region PROPERTIES
    /** @var DBCacheContract $cacheRecorder */
    private $dbCacheService;
    #endregion

    #region MAIN METHODS
    /**
     * UserObserver constructor.
     * @param DBCacheContract $dbCacheService
     */
    public function __construct(DBCacheContract $dbCacheService)
    {
        $this->dbCacheService = $dbCacheService;
    }

    /**
     * @param Model $user
     */
    public function saved(Model $user): void
    {
        $this->loadUserRelations($user);
        $this->saveUser($user);
    }

    /**
     * @param Model $user
     */
    public function deleted(Model $user):void
    {
        $this->loadUserRelations($user);
        $this->dbCacheService->deleteModel($user, self::USER_ALL);
        $this->dbCacheService->deleteModel($user, self::USER_WITHOUT_ROLE);
        $this->dbCacheService->deleteModel($user, self::USER_NOT_APPROVED);
        $this->dbCacheService->deleteModel($user, self::USER_NOT_VERIFIED);
        foreach ($this->getRealRoles() as $role) {
            $this->dbCacheService->deleteModel($user, "user:{$role}");
        }
        $this->dbCacheService->saveModel($user, self::USER_DELETED);
    }

    public function refreshed(): void
    {
        // TODO: Implement refreshed() method. Queue job is required here
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param User $user
     */
    private function loadUserRelations(User $user): void
    {
        $user->load('roles');
    }

    /**
     * @param User $user
     */
    private function saveUser(User $user): void
    {
        $this->handleSavedUserAllStatus($user);
        $this->handleSavedUserApprovalStatus($user);
        $this->handleSavedUserVerificationStatus($user);
        $this->handleSavedUserRolesQtyStatus($user);
        $this->updateSavedUserRolesModels($user);
        $this->handleSavedUserDeleteStatus($user);
    }

    private function handleSavedUserAllStatus(User $user): void
    {
        if (!$user->trashed()) {
            $this->dbCacheService->saveModel($user, self::USER_ALL);
        } else {
            $this->dbCacheService->deleteModel($user, self::USER_ALL);
        }
    }

    /**
     * @param User $user
     */
    private function handleSavedUserApprovalStatus(User $user): void
    {
        if (!$user->isApproved() && !$user->trashed()) {
            $this->dbCacheService->saveModel($user, self::USER_NOT_APPROVED);
        } else {
            $this->dbCacheService->deleteModel($user, self::USER_NOT_APPROVED);
        }
    }

    /**
     * @param User $user
     */
    private function handleSavedUserVerificationStatus(User $user): void
    {
        if (!$user->isVerified() && !$user->trashed()) {
            $this->dbCacheService->saveModel($user, self::USER_NOT_VERIFIED);
        } else {
            $this->dbCacheService->deleteModel($user, self::USER_NOT_VERIFIED);
        }
    }

    /**
     * @param User $user
     */
    private function handleSavedUserRolesQtyStatus(User $user): void
    {
        if (count($user->roles) === 0 && !$user->trashed()) {
            $this->dbCacheService->saveModel($user, self::USER_WITHOUT_ROLE);
        } else {
            $this->dbCacheService->deleteModel($user, self::USER_WITHOUT_ROLE);
        }
    }

    /**
     * @param User $user
     */
    private function updateSavedUserRolesModels(User $user): void
    {
        foreach ($this->getRealRoles() as $roleId => $role) {
            if (!empty($user->roles()->find($roleId)) && !$user->trashed()) {
                $this->dbCacheService->saveModel($user, "user:{$role}");
            } else {
                $this->dbCacheService->deleteModel($user, "user:{$role}");
            }
        }
    }

    private function handleSavedUserDeleteStatus(User $user): void
    {
        if (!$user->trashed()) {
            $this->dbCacheService->deleteModel($user, self::USER_DELETED);
        } else {
            $this->dbCacheService->saveModel($user, self::USER_DELETED);
        }
    }
    #endregion
}
