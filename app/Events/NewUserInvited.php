<?php
/**
 * User: Andrey Naumoff
 * Date: 14-Feb-19
 * Time: 10:46 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Events;

use App\Models\UserModels\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class NewUserInvited
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var User $user */
    public $user;

    /** @var string $temporaryPassword */
    public $temporaryPassword;

    /**
     * NewUserInvited constructor.
     * @param User $user
     * @param string $temporaryPassword
     */
    public function __construct(User $user, string $temporaryPassword)
    {
        $this->user = $user;
        $this->temporaryPassword = $temporaryPassword;
    }
}
