<?php

use App\Models\PageModels\Page;
use App\Services\DataHelpers\MenuPageBooleans;
use Illuminate\Database\Migrations\Migration;

class AddBooleanFieldsToMenuPages extends Migration
{
    use MenuPageBooleans;

    #region MAIN METHODS
    /**
     * Run the migrations.
     * @SuppressWarnings(PHPMD)
     * @return void
     */
    public function up()
    {
        $this->addBooleansToMainPage();
        $this->addBooleansToContactPage();
        $this->addBooleansToAboutUsPage();
        $this->addBooleansToServicesPage();
        $this->addBooleansToOurWorkPage();
        $this->addBooleansToNewsPage();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->removeBooleansFromMainPage();
        $this->removeBooleansFromContactPage();
        $this->removeBooleansFromAboutUsPage();
        $this->removeBooleansFromServicesPage();
        $this->removeBooleansFromOurWorkPage();
        $this->removeBooleansFromNewsPage();
    }
    #endregion

    #region SERVICE METHODS
    private function addBooleansToMainPage(): void
    {
        /** @var Page $mainPage */
        $mainPage = Page::find(Page::MAIN);
        $mainPage->booleanFields()->createMany($this->mainPageBooleans);
        $mainPage->touch();
    }

    private function addBooleansToContactPage(): void
    {
        /** @var Page $contactPage */
        $contactPage = Page::find(Page::CONTACTS);
        $contactPage->booleanFields()->createMany($this->contactsPageBooleans);
        $contactPage->touch();
    }

    private function addBooleansToAboutUsPage(): void
    {
        /** @var Page $aboutUsPage */
        $aboutUsPage = Page::find(Page::ABOUT_US);
        $aboutUsPage->booleanFields()->createMany($this->aboutUsPageBooleans);
        $aboutUsPage->touch();
    }

    private function addBooleansToServicesPage(): void
    {
        /** @var Page $servicesPage */
        $servicesPage = Page::find(Page::SERVICES);
        $servicesPage->booleanFields()->createMany($this->servicesPageBooleans);
        $servicesPage->touch();
    }

    private function addBooleansToOurWorkPage(): void
    {
        /** @var Page $ourWorkPage */
        $ourWorkPage = Page::find(Page::OUR_WORK);
        $ourWorkPage->booleanFields()->createMany($this->ourWorkPageBooleans);
        $ourWorkPage->touch();
    }

    private function addBooleansToNewsPage(): void
    {
        /** @var Page $newsPage */
        $newsPage = Page::find(Page::NEWS);
        $newsPage->booleanFields()->createMany($this->newsPageBooleans);
        $newsPage->touch();
    }

    private function removeBooleansFromMainPage(): void
    {
        /** @var Page $mainPage */
        $mainPage = Page::find(Page::MAIN);
        foreach ($this->mainPageBooleans as $boolean) {
            $mainPage->booleanFields()->where(['name' => $boolean['name']])->delete();
        }
        $mainPage->touch();
    }

    private function removeBooleansFromContactPage(): void
    {
        /** @var Page $contactPage */
        $contactPage = Page::find(Page::CONTACTS);
        foreach ($this->contactsPageBooleans as $boolean) {
            $contactPage->booleanFields()->where(['name' => $boolean['name']])->delete();
        }
        $contactPage->touch();
    }

    private function removeBooleansFromAboutUsPage(): void
    {
        /** @var Page $aboutUsPage */
        $aboutUsPage = Page::find(Page::ABOUT_US);
        foreach ($this->aboutUsPageBooleans as $boolean) {
            $aboutUsPage->booleanFields()->where(['name' => $boolean['name']])->delete();
        }
        $aboutUsPage->touch();
    }

    private function removeBooleansFromServicesPage(): void
    {
        /** @var Page $servicesPage */
        $servicesPage = Page::find(Page::SERVICES);
        foreach ($this->servicesPageBooleans as $boolean) {
            $servicesPage->booleanFields()->where(['name' => $boolean['name']])->delete();
        }
        $servicesPage->touch();
    }

    private function removeBooleansFromOurWorkPage(): void
    {
        /** @var Page $ourWorkPage */
        $ourWorkPage = Page::find(Page::OUR_WORK);
        foreach ($this->ourWorkPageBooleans as $boolean) {
            $ourWorkPage->booleanFields()->where(['name' => $boolean['name']])->delete();
        }
        $ourWorkPage->touch();
    }

    private function removeBooleansFromNewsPage(): void
    {
        /** @var Page $newsPage */
        $newsPage = Page::find(Page::NEWS);
        foreach ($this->newsPageBooleans as $boolean) {
            $newsPage->booleanFields()->where(['name' => $boolean['name']])->delete();
        }
        $newsPage->touch();
    }
    #endregion
}
