<?php

use App\Contracts\LocaleContract;
use App\Models\PageModels\MetadataField;
use App\Models\PageModels\Page;
use App\Models\PageModels\RawHtmlField;
use App\Services\DataHelpers\SliderSinglePages;
use Illuminate\Database\Migrations\Migration;

class AddSinglePagesToSlider extends Migration
{
    use SliderSinglePages;

    #region MAIN METHODS
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        foreach ($this->singlePagesForSlider as $singlePage) {
            /** @var Page $page */
            $page = Page::create($singlePage);
            $this->addMetaTags($page);
            $this->addRawHtml($page);
            $page->touch();
        }
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        foreach ($this->singlePagesForSlider as $singlePage) {
            /** @var Page $page */
            $page = Page::where('name', $singlePage['name'])->first();
            $page->delete();
        }
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param Page $page
     */
    private function addMetaTags(Page $page)
    {
        $pageMetatags = new MetadataField();
        $pageMetatags->page_id = $page->id;
        foreach (LocaleContract::AVAILABLE_LOCALES as $locale) {
            $header = 'header_'.$locale;
            $title = 'title_'.$locale;
            $pageMetatags->$header = $this->singlePagesMetatags[$page->name][$header];
            $pageMetatags->$title = $this->singlePagesMetatags[$page->name][$title];
        }
        $pageMetatags->save();
    }

    /**
     * @param Page $page
     */
    private function addRawHtml(Page $page)
    {
        $pageRawHtmls = new RawHtmlField();
        $pageRawHtmls->page_id = $page->id;
        $pageRawHtmls->name = $this->singlePagesRawHtmls[$page->name]['name'];
        $pageRawHtmls->description = $this->singlePagesRawHtmls[$page->name]['description'];
        foreach (LocaleContract::AVAILABLE_LOCALES as $locale) {
            $pageRawHtmls->$locale = $this->singlePagesRawHtmls[$page->name][$locale];
        }
        $pageRawHtmls->save();
    }
    #endregion
}
