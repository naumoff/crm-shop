<?php

use App\Contracts\LocaleContract;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRawHtmlFieldsTable extends Migration
{
    /**
     * Run the migrations.
     * @SuppressWarnings(PHPMD)
     * @return void
     */
    public function up()
    {
        Schema::create('raw_html_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 25);
            $table->string('description')->nullable();
            foreach (LocaleContract::AVAILABLE_LOCALES as $locale) {
                $table->longText($locale);
            }
            $table->integer('page_id')->unsigned()->index();
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raw_html_fields');
    }
}
