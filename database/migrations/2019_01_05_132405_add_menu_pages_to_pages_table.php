<?php

use App\Models\PageModels\Page;
use App\Models\PageModels\PageType;
use App\Services\DataHelpers\MenuPages;
use Illuminate\Support\Carbon;
use Illuminate\Database\Migrations\Migration;

class AddMenuPagesToPagesTable extends Migration
{
    use MenuPages;

    #region MAIN METHODS
    /**
     * Run the migrations.
     * @SuppressWarnings(PHPMD)
     * @return void
     */
    public function up()
    {
        foreach ($this->menuPageNames as $id => $menuPageName) {
            $insertData = [
                'id' => $id,
                'name' => $menuPageName,
                'page_type_id' => PageType::MENU,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
            Page::create($insertData);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $pagesIds = $this->getMenuPageIds();
        Page::destroy($pagesIds);
    }
    #endregion
}
