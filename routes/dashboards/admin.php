<?php
/**
 * User: Andrey Naumoff
 * Date: 22-Nov-18
 * Time: 3:26 PM
 * E-mail: andrey.naumoff@gmail.com
 */

use App\Models\UserModels\Role;

Route::group(
    [
        'middleware' => ['role:' . Role::ADMIN, 'approved', 'auth'],
        'prefix' => '/admin'
    ],
    function () {
        Route::get('/', 'AdminController@welcome')->name('admin');

        Route::get('/users/invitation', 'AdminController@userInvite')->name('user-invite');

        Route::post('/users/invitation', 'AdminController@userCreate')->name('user-create');

        Route::get('/users/{userStatus}', 'AdminController@users')->name('users')
            ->where(['userStatus' => '[a-z-]+']);

        Route::delete('/users/{user}', 'AdminController@userDestroy')->name('user-destroy')
            ->where(['user' => '[0-9]+']);

        Route::patch('/users/{deletedUser}/restore', 'AdminController@userRestore')->name('user-restore')
            ->where(['user' => '[0-9]+']);

        Route::patch('/users/{userWithTrashed}', 'AdminController@userUpdate')->name('user-update')
            ->where(['user' => '[0-9]+']);

        Route::get('users/{user}/restore', 'AdminController@userRestore')->name('user-restore')
            ->where(['user' => '[0-9]+']);
    }
);
