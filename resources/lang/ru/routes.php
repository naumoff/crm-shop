<?php

return [
    'main' => 'Главная',
    'about-us' => 'О нас',
    'services' => 'Услуги',
    'our-work' => 'Наши работы',
    'news' => 'Новости',
    'contacts' => 'Контакты',
];
