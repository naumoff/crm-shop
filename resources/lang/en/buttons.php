<?php

return [
    'get-quote' => 'Get a quote',
    'send-message' => 'Send message',
    'send' => 'Send',
    'go-home-page' => 'go to home page',
    'contact-us' => 'contact us',
];
