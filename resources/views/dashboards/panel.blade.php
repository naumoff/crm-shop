@extends('dashboards.layouts.dashboard')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboards</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h3>Available Dashboards</h3>
                        @if (Auth::user()->isApproved())
                            @if(Auth::user()->isAdmin())
                                <a href="{{route('admin')}}" class="btn btn-primary btn-block" role="button">Site Administration</a>
                            @endif
                            @if(Auth::user()->isAdmin() || Auth::user()->isContentManager())
                                <a href="{{route('cms-manager')}}" class="btn btn-secondary btn-block" role="button">Content Management</a>
                            @endif
                            @if(Auth::user()->isAdmin() || Auth::user()->isContentManager() || Auth::user()->isContentEditor())
                                <a href="{{route('cms-editor')}}" class="btn btn-secondary btn-block" role="button">Content Editorial</a>
                            @endif
                        @endif
                        <a href="{{route('main')}}" class="btn btn-secondary btn-block" role="button">Exit</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
