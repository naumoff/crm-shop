@extends('dashboards.layouts.dashboard')

@section('content')
    <!-- .page -->
    <div class="page">
        <!-- .page-inner -->
        <div class="page-inner">
            <!-- .page-title-bar -->
            <header class="page-title-bar">
                <!-- page title stuff goes here -->
                <h1 class="page-title">Metatags for '{{$pageModel->name}}' menu page</h1>
            </header>
            <!-- /.page-title-bar -->
            <!-- .page-section -->
            <div class="page-section">
                <!-- .section-deck -->
                <div class="section-deck">
                    <!-- .card -->
                    <section class="card card-fluid">
                        <!-- .card-header -->
                        <header class="card-header">
                            <!-- .nav-tabs -->
                            <ul class="nav nav-tabs card-header-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active show" data-toggle="tab" href="#title">Title</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#description">Description</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#keywords">Keywords</a>
                                </li>
                            </ul><!-- /.nav-tabs -->
                        </header><!-- /.card-header -->
                        <!-- .card-body -->
                        <div class="card-body">
                            <!-- .tab-content -->
                            <div id="myTabContent" class="tab-content">
                                <div class="tab-pane fade active show" id="title">
                                    @include('dashboards.cms.manager.inclusions.menu_page_title_form')
                                </div>
                                <div class="tab-pane fade" id="description">
                                    @include('dashboards.cms.manager.inclusions.menu_page_description_form')
                                </div>
                                <div class="tab-pane fade" id="keywords">
                                    @include('dashboards.cms.manager.inclusions.menu_page_keywords_form')
                                </div>
                            </div><!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </section><!-- /.card -->
                </div><!-- /.section-deck -->
                <!-- page content goes here -->
            </div>
            <!-- /.page-section -->
        </div>
        <!-- /.page-inner -->
    </div>
@endsection