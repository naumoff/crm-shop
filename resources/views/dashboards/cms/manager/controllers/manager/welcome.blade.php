@extends('dashboards.layouts.dashboard')

@section('content')
    <!-- .page -->
    <div class="page">
        <!-- .page-inner -->
        <div class="page-inner">
            <!-- .page-title-bar -->
            <header class="page-title-bar">
                <!-- page title stuff goes here -->
                <h1 class="page-title">Welcome to cms-management dashboard, {{ Auth::user()->name }}!</h1>
            </header>
            <!-- /.page-title-bar -->

            <!-- .page-section -->
            <div class="page-section">
                <!-- page content goes here -->
                <p></p>
            </div>
            <!-- /.page-section -->
        </div>
        <!-- /.page-inner -->
    </div>
@endsection