<div class="row">
    <div class="col-xl-8">
        <section class="card card-fluid">
            <!-- .card-header -->
            <header class="card-header border-0">
                <div class="d-flex align-items-center">
                    <span class="mr-auto">Access Management table to {{ ucfirst($pageModel->name) }} page:</span>
                    <div class="dropdown">
                        <button type="button" class="btn btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                        <div class="dropdown-arrow"></div>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Archive</a> <a href="#!" class="dropdown-item">Remove</a>
                        </div>
                    </div>
                </div>
            </header><!-- /.card-header -->
            <!-- .table-responsive -->
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead class="thead-">
                    <tr>
                        <th> Id </th>
                        <th> Variable Input </th>
                        <th> Show / Hide </th>
                        <th style="width:100px; min-width:100px;"> &nbsp; </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($pageModel->raw_html_fields as $rawHtmlField)
                        <tr>
                            <td> {{ $rawHtmlField->id }} </td>
                            <td> {{ $rawHtmlField->name }} </td>
                            <td>
                                <div class="list-group-item d-flex justify-content-between align-items-center">
                                    <span>Show / Hide</span> <!-- .switcher-control -->
                                    <label class="switcher-control switcher-control-success">
                                        <input
                                            type="checkbox"
                                            class="switcher-input menu-page-access"
                                            checked
                                        >
                                        <span class="switcher-indicator"></span>
                                    </label>
                                </div>
                            </td>
                            <td class="align-middle text-right">
                                <a
                                    href="#!"
                                    class="btn btn-sm btn-icon btn-secondary field-modal"
                                    data-toggle="modal"
                                    data-target="#editField"
                                    data-page-id="{{ $pageModel->id }}"
                                    data-field-id="{{ $rawHtmlField->id }}"
                                    data-field-name="{{ $rawHtmlField->name }}"
                                    data-field-en="{{$rawHtmlField->en}}"
                                    data-field-ru="{{$rawHtmlField->ru}}"
                                    data-field-ua="{{$rawHtmlField->ua}}"
                                >
                                    <i class="fa fa-pencil-alt"></i>
                                    <span class="sr-only">Edit</span>
                                </a>
                                <a href="#!" class="btn btn-sm btn-icon btn-secondary">
                                    <i class="far fa-trash-alt"></i> <span class="sr-only">Remove</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>

    <!-- The Modal -->
    <div class="modal fade" id="editField">
        <div class="modal-dialog" style="max-width: 1800px">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Modal Heading</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div id="accordion" class="card-expansion">
                            <form method="post" action="{{ route('regular-page-raw-html-patch') }}">
                                @foreach(\App\Contracts\LocaleContract::AVAILABLE_LOCALES as $key => $locale)
                                    <section class="card card-expansion-item">
                                        <header class="card-header border-0" id="heading-{{$locale}}">
                                            <button class="btn btn-reset collapsed not-form" data-toggle="collapse" data-target="#collapse-{{$locale}}" aria-expanded="false" aria-controls="collapse-{{$locale}}">
                                                <span class="collapse-indicator mr-2">
                                                  <i class="fa fa-fw fa-caret-right"></i>
                                                </span>
                                                <span>Data for locale {{$locale}}</span>
                                            </button>
                                            <span class="badge badge-danger">Required</span>
                                        </header>
                                        <div id="collapse-{{$locale}}" class="collapse" aria-labelledby="heading-{{$locale}}" data-parent="#accordion">
                                            <div class="card-body pt-0">
                                                <div class="form-group">
                                                    <label for="lbl2">Field content for locale <b>{{$locale}}</b></label>
                                                    <textarea
                                                            rows="15"
                                                            name="{{$locale}}"
                                                            class="form-control"
                                                            id="textarea-{{$locale}}"
                                                            placeholder="Required description"
                                                            required="required"
                                                    ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                @endforeach
                                @csrf
                                @method('PATCH')
                                <input type="text" name="page_id" id="page-id" hidden>
                                <input type="text" name="field_id" id="field-id" hidden>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </form>
                        </div>
                    </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger save-field" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')
    <script>
        $(document).ready(function(){
            $(".field-modal").click(function(){
                let pageId = $(this).data('page-id');
                let fieldId = $(this).data('field-id');
                let fieldName = $(this).data('field-name');
                let fieldContentEn = $(this).data('field-en');
                let fieldContentRu = $(this).data('field-ru');
                let fieldContentUa = $(this).data('field-ua');
                $(".modal-title").text(fieldName);
                $("#textarea-en").text(fieldContentEn);
                $("#textarea-ru").text(fieldContentRu);
                $("#textarea-ua").text(fieldContentUa);
                $("#page-id").val(pageId);
                $("#field-id").val(fieldId);
            });
            $(".not-form").click(function(e){
                e.preventDefault();
            });
        })
    </script>
@endpush