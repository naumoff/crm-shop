<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="userEditModal" tabindex="-1" role="dialog" aria-labelledby="userEditTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="userEditTitle">User Name</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-12 error-block">
                    <div class="alert alert-danger alert-dismissible fade show">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>Validation error!</strong>
                        <ul class="error-list">
                        </ul>
                    </div>
                </div>
                <!-- .card -->
                <div class="card">
                    <!-- .card-body -->
                    <div class="card-body">
                        <h4 class="card-title"> User Info </h4><!-- form .needs-validation -->
                        <form class="needs-validation" id="user-update-modal" novalidate="" method="post">
                            <!-- .form-row -->
                            <div class="form-row">
                                <!-- grid column -->
                                <div class="col-md-6 mb-3">
                                    <label for="userName">User name</label>
                                    <input type="text" class="form-control" id="userName" required="" value="">
                                    <div class="invalid-feedback"> Valid user name is required. </div>
                                </div><!-- /grid column -->
                                <!-- grid column -->
                                <div class="col-md-6 mb-3">
                                    <label for="userEmail">User email</label>
                                    <input type="email" class="form-control" id="userEmail" required="" value="">
                                    <div class="invalid-feedback"> Valid email is required. </div>
                                </div><!-- /grid column -->
                            </div><!-- /.form-row -->

                            <!-- .form-row -->
                            <div class="form-row">
                                <!-- grid column -->
                                <div class="col-md-6 mb-3">
                                    <label for="userPhone">
                                        User phone
                                        <span class="badge badge-secondary">
                                                <em>Optional</em>
                                            </span>
                                    </label>
                                    <input type="text" class="form-control" id="userPhone" value="" placeholder="Enter user's phone number here">
                                    <div class="invalid-feedback"> Valid phone number required. </div>
                                </div><!-- /grid column -->
                                <!-- grid column -->
                                <div class="col-md-6 mb-3">
                                    <label for="userLocation">
                                        User location
                                        <span class="badge badge-secondary">
                                            <em>Optional</em>
                                        </span>
                                    </label>
                                    <input type="text" class="form-control" id="userLocation" value="" placeholder="Enter country, region and post code here">
                                    <div class="invalid-feedback"> Valid user location is required. </div>
                                </div><!-- /grid column -->
                            </div><!-- /.form-row -->

                            <!-- .form-group -->
                            <div class="form-group">
                                <label for="userAddress">
                                    Address
                                    <span class="badge badge-secondary">
                                            <em>Optional</em>
                                        </span>
                                </label>
                                <input type="text" class="form-control" id="userAddress" value="" placeholder="Enter city/town/village, street name, house number, apartment number here">
                            </div><!-- /.form-group -->

                            <hr class="mb-4">
                            <h4 class="card-title"> Roles </h4>
                            <!-- .form-group -->
                            <div class="form-group">
                            @foreach($roles as $roleId => $roleName)
                                <!-- .custom-control -->
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input user-roles" id="role-{{ $roleId }}" value="{{ $roleId }}">
                                        <label class="custom-control-label" for="role-{{ $roleId }}">
                                            {{ $roleName }}
                                        </label>
                                    </div><!-- /.custom-control -->
                                @endforeach
                            </div><!-- /.form-group -->
                            <hr class="mb-4">
                            <h4 class="card-title"> Approval </h4><!-- .form-group -->
                            <!-- .form-group -->
                            <div class="list-group-item d-flex justify-content-between align-items-center">
                                <span>User approved?</span>
                                <div>
                                    <!-- .switcher-control -->
                                    <label class="switcher-control switcher-control-success switcher-control-lg">
                                        <input type="checkbox" id="user-approved" name="user-approved" class="switcher-input" value="1">
                                        <span class="switcher-indicator"></span>
                                        <span class="switcher-label-on">
                                                <i class="fas fa-check"></i>
                                            </span>
                                        <span class="switcher-label-off">
                                                <i class="fas fa-times"></i>
                                            </span>
                                    </label> <!-- /.switcher-control -->
                                    <!-- .switcher-control -->
                                </div>
                            </div><!-- /.list-group-item -->
                            <!-- .list-group-item -->

                        </form><!-- /form .needs-validation -->
                    </div><!-- /.card-body -->
                </div><!-- /.card -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary save-user">Save changes</button>
            </div>
        </div>
    </div>
</div>
