<header class="aside-header d-block d-md-none">
    <!-- .btn-account -->
    <button class="btn-account" type="button" data-toggle="collapse" data-target="#dropdown-aside">
            <span class="user-avatar user-avatar-lg">
                {{--<img src="assets/images/avatars/profile.jpg" alt="">--}}
            </span>
        <span class="account-icon">
                <span class="fa fa-caret-down fa-lg"></span>
            </span>
        <span class="account-summary">
                <span class="account-name">{{ Auth::user()->name }}</span>
                <span class="account-description">{{ Auth::user()->roles->first()->role ?? 'no role yet' }}</span>
            </span>
    </button> <!-- /.btn-account -->
    <!-- .dropdown-aside -->
    <div id="dropdown-aside" class="dropdown-aside collapse">
        <!-- dropdown-items -->
        <div class="pb-3">
            <a class="dropdown-item" href="user-profile.html">
                <span class="dropdown-icon oi oi-person"></span> Profile
            </a>
            <a class="dropdown-item"
               href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                <span class="dropdown-icon oi oi-account-logout"></span> Logout
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#!">Help Center</a>
            <a class="dropdown-item" href="#!">Ask Forum</a>
        </div><!-- /dropdown-items -->
    </div><!-- /.dropdown-aside -->
</header><!-- /.aside-header -->