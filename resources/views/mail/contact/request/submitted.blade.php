@component('mail::message')
# Contact Request Submitted

<hr>

<p>User with name {{ $user }} applied for contact request:</p>

<p><b>message:</b> {{ $message }}</p>
<p><b>phone:</b> {{ $phone }}</p>
<p><b>email:</b> {{ $email }}</p>
<p><b>site locale:</b> {{ $locale }}</p>

{{--URL will defined later to admin table with contact requests--}}
{{--@component('mail::button', ['url' => ''])--}}
{{--Button Text--}}
{{--@endcomponent--}}

<hr>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
