@component('mail::message')
# Subscription attempt notification
<hr>
<p>{{$user}} submitted subscription request on email <b>{{$email}}</b>.</p>
<p>During subscribe request submission preferred locale was <b>"{{$locale}}"</b>.</p>
<hr>
{{--Button will be added after admin representations will be complited--}}
{{--@component('mail::button', ['url' => ''])--}}
{{--Button Text--}}
{{--@endcomponent--}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
