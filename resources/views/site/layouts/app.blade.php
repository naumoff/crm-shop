<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
<head>
    <!-- Site Title-->
    <title>{{ (!empty($seo->title))? $seo->title : env('APP_NAME') }}</title>
    <meta name="description" content="{{ (!empty($seo->description))? $seo->description : null }}">
    <meta name="keywords" content="{{ (!empty($seo->keywords))? $seo->keywords : null }}">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="/site/images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=PT+Sans:300,400,500,700%7CRaleway:700">
    <link rel="stylesheet" href="{{ url('/site/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('/site/css/style.css') }}">
</head>
<body>
<!-- Page Loader-->
@include('site.inclusions.page_loader')
<!-- Page-->
<div class="page">
    <!-- Page Header-->
    @include('site.inclusions.header')

    <!-- Content -->
    @yield('content')

    <!-- Page Footer-->
    @include('site.inclusions.footer')

</div>
<!-- Global Mailform Output-->
<div class="snackbars" id="form-output-global"></div>
<!-- Javascript-->
<script src="{{ url('/site/js/core.min.js') }}"></script>
<script src="{{ url('/site/js/script.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $('.lang-switch').on('click', function () {
            $('.button-quote').css('margin-right', '38px');
        })
    });
</script>
</body>
</html>