@extends('site.layouts.app')

@section('content')
    @if($fields->booleans->showBreadCrumbs)
        <!-- Breadcrumbs-->
        <section class="breadcrumbs-custom bg-image" style="background-image: url(/site/images/bg-image-9.jpg);">
            <div class="shell">
                <h2 class="breadcrumbs-custom__title">{{$menuPageNames->news ?? __('routes.news')}}</h2>
                <ul class="breadcrumbs-custom__path">
                    <li><a href="{{route('main', ['locale'=>App::getLocale()])}}">{{$menuPageNames->main ?? __('routes.main')}}</a></li>
                    <li class="active">{{$menuPageNames->news ?? __('routes.news')}}</li>
                </ul>
            </div>
        </section>
    @endif

    <!-- News-->
    <section class="section section-md bg-gray-4 text-center">
        <div class="shell">
            <div class="range range-30">
                <div class="cell-sm-6 cell-md-4">
                    <article class="post-modern"><a class="post-modern__image-wrap" href="single-post.html"><img class="post-modern__image" src="/site/images/post-1-365x215.jpg" alt="" width="365" height="215"/></a>
                        <div class="post-modern__main">
                            <p class="post-modern__title"><a href="single-post.html">How We Measure the Increasing Vlogs' Ad Potential</a></p>
                            <p class="post-modern__text">We have recently finished our research, which is closely connected</p>
                            <ul class="post-modern__meta">
                                <li><span class="icon icon-primary mdi mdi-clock"></span><a href="single-post.html">January 6, 2017</a></li>
                                <li><span class="icon icon-primary fl-justicons-visible6"></span><span>524</span></li>
                                <li> <span class="icon icon-primary mdi mdi-comment-outline"></span><a href="single-post.html">3</a></li>
                            </ul>
                        </div>
                    </article>
                </div>
                <div class="cell-sm-6 cell-md-4">
                    <article class="post-modern"><a class="post-modern__image-wrap" href="single-post.html"><img class="post-modern__image" src="/site/images/post-2-365x215.jpg" alt="" width="365" height="215"/></a>
                        <div class="post-modern__main">
                            <p class="post-modern__title"><a href="single-post.html">Digital Marketing Metrics That Really Matter Nowadays</a></p>
                            <p class="post-modern__text">Marketing is currently in a state of evolution where it is dispensing</p>
                            <ul class="post-modern__meta">
                                <li><span class="icon icon-primary mdi mdi-clock"></span><a href="single-post.html">January 6, 2017</a></li>
                                <li><span class="icon icon-primary fl-justicons-visible6"></span><span>524</span></li>
                                <li> <span class="icon icon-primary mdi mdi-comment-outline"></span><a href="single-post.html">3</a></li>
                            </ul>
                        </div>
                    </article>
                </div>
                <div class="cell-sm-6 cell-md-4">
                    <article class="post-modern"><a class="post-modern__image-wrap" href="single-post.html"><img class="post-modern__image" src="/site/images/post-5-365x215.jpg" alt="" width="365" height="215"/></a>
                        <div class="post-modern__main">
                            <p class="post-modern__title"><a href="single-post.html">How We Can Help eCommerce Marketers</a></p>
                            <p class="post-modern__text">If you are an eCommerce marketer, you are probably looking for ways of</p>
                            <ul class="post-modern__meta">
                                <li><span class="icon icon-primary mdi mdi-clock"></span><a href="single-post.html">January 6, 2017</a></li>
                                <li><span class="icon icon-primary fl-justicons-visible6"></span><span>524</span></li>
                                <li> <span class="icon icon-primary mdi mdi-comment-outline"></span><a href="single-post.html">3</a></li>
                            </ul>
                        </div>
                    </article>
                </div>
                <div class="cell-sm-6 cell-md-8">
                    <article class="post-modern"><a class="post-modern__image-wrap" href="single-post.html"><img class="post-modern__image" src="/site/images/post-4-764x215.jpg" alt="" width="764" height="215"/></a>
                        <div class="post-modern__main">
                            <p class="post-modern__title"><a href="single-post.html">Email Newsletters Hit a Speed Bump with Inaccurate Open Rates</a></p>
                            <p class="post-modern__text">As Clover Letter Emails Got Bigger in size, Founders Say, Its Open Rate Collapsed. Email newsletters are just as effective when working with existing customers, as they were from the very start.Also, the part of the problem...</p>
                            <ul class="post-modern__meta">
                                <li><span class="icon icon-primary mdi mdi-clock"></span><a href="single-post.html">January 6, 2017</a></li>
                                <li><span class="icon icon-primary fl-justicons-visible6"></span><span>524</span></li>
                                <li> <span class="icon icon-primary mdi mdi-comment-outline"></span><a href="single-post.html">3</a></li>
                            </ul>
                        </div>
                    </article>
                </div>
                <div class="cell-sm-6 cell-md-4">
                    <article class="post-modern"><a class="post-modern__image-wrap" href="single-post.html"><img class="post-modern__image" src="/site/images/post-5-365x215.jpg" alt="" width="365" height="215"/></a>
                        <div class="post-modern__main">
                            <p class="post-modern__title"><a href="single-post.html">How We Can Help eCommerce Marketers</a></p>
                            <p class="post-modern__text">If you are an eCommerce marketer, you are probably looking for ways of</p>
                            <ul class="post-modern__meta">
                                <li><span class="icon icon-primary mdi mdi-clock"></span><a href="single-post.html">January 6, 2017</a></li>
                                <li><span class="icon icon-primary fl-justicons-visible6"></span><span>524</span></li>
                                <li> <span class="icon icon-primary mdi mdi-comment-outline"></span><a href="single-post.html">3</a></li>
                            </ul>
                        </div>
                    </article>
                </div>
            </div>
            <!-- Pagination-->
            <ul class="pagination-custom">
                <li class="pagination-control"><a href="#">Prev</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li class="active"><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li class="pagination-control"><a href="#">Next</a></li>
            </ul>
        </div>
    </section>
@endsection
