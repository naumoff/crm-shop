@extends('site.layouts.app')

@section('content')
    <section class="breadcrumbs-custom bg-image" style="background-image: url(/site/images/bg-image-1.jpg);">
        <div class="shell">
            <p class="heading-1 breadcrumbs-custom__title">404</p>
        </div>
    </section>

    <!-- 404-->
    <section class="section section-sm bg-white text-center">
        <div class="shell">
            <div class="range range-sm-center">
                <div class="cell-lg-7">
                    <h2>@lang('strings.404-sorry')</h2>
                    <p class="large">@lang('strings.404-reason')</p>
                    <div class="group-sm group-sm_mod-1">
                        @can(\App\Contracts\SitePageAccessContract::VIEW_MENU_PAGE, \App\Models\PageModels\Page::MAIN)
                            <a class="button button-darker" href="{{route('main', ['locale' => App::getLocale()])}}">
                                @lang('buttons.go-home-page')
                            </a>
                        @endcan
                        @can(\App\Contracts\SitePageAccessContract::VIEW_MENU_PAGE, \App\Models\PageModels\Page::CONTACTS)
                            <p class="text-bold">@lang('strings.or')</p>
                            <a class="button button-primary" href="{{route('contacts', ['locale' => App::getLocale()])}}">
                                @lang('buttons.contact-us')
                            </a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection