@extends('site.layouts.app')

@section('content')
    <!-- Breadcrumbs-->
    <section class="breadcrumbs-custom bg-image" style="background-image: url(/site/images/bg-image-1.jpg);">
        <div class="shell">
            <h2 class="breadcrumbs-custom__title">{{ $seo->header ?? $seo->title }}</h2>
            <ul class="breadcrumbs-custom__path">
                <li><a href="{{route('main', ['locale'=>App::getLocale()])}}">{{$menuPageNames->main ?? __('routes.main')}}</a></li>
                <li class="active">{{ $seo->header ?? $seo->title }}</li>
            </ul>
        </div>
    </section>



    <section class="section section-md bg-white">
        {!! $fields->htmls->pageInput !!}

        {{-- SECURITY/INTEGRATION STRUCTURE PART --}}
        {{--<div class="shell">--}}
            {{--<div class="range range-70 range-sm-center range-lg-justify">--}}
                {{--<div class="cell-sm-10 cell-md-8 cell-lg-8">--}}
                    {{--<h4>DEVELOP YOUR COMPLEX DATA SYSTEM WITH OPENSOURCE</h4>--}}
                    {{--<p>--}}
                        {{--OPEN SOURCE ENTERPRISE SOLUTIONS for Companies working with big volume dynamic data bases.--}}
                        {{--All in one - from back end to front end.--}}

                        {{--Example:--}}
                        {{--.Virtual servers - 350--}}
                        {{--.System is divided in 3 vertical equal zones--}}
                        {{--.Files’ volume in object storage - 80 Tb. Increasing + 3,5 Tb/month--}}
                        {{--.Volume of NoSQL productive data base - 230 Gb--}}
                        {{--.200 000 system users--}}
                        {{--.Quantity of fixed sessions per day - 21 mln.--}}
                        {{--.Traffic per day: 442 Gb downloaded, 88 Gb uploaded--}}

                        {{--SYSTEM OF GENERAL MONITORING, ALERT, LOGGING, RESERVATION and LOADS BALANCING guarantees Your safety and replaces usual SLAs of Vendors.--}}

                        {{--Example:--}}
                        {{--Monitoring system provides quick response thanks to ready-made scripts and settings, graphs, dashboards, as well as alerts, configured for triggers by system levels and services.--}}
                        {{--Logging system ensures transparency of all changes in the system and the possibility of complete audit.--}}
                        {{--Load balancing ensures functioning of systems by distributing large loads.--}}
                    {{--</p>--}}
                {{--</div>--}}
                {{--<div class="cell-sm-10 cell-md-4">--}}
                    {{--<div class="row grid-2">--}}
                        {{--<div class="col-xs-6"><img src="/site/images/about-1-273x214.jpg" alt="" width="273" height="214"/><img src="/site/images/about-2-273x214.jpg" alt="" width="273" height="214"/>--}}
                        {{--</div>--}}
                        {{--<div class="col-xs-6"><img src="/site/images/about-3-273x451.jpg" alt="" width="273" height="451"/>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{-- DEVELOPMENT PART --}}
        {{--<div class="shell">--}}
            {{--<div class="range range-70 range-sm-center range-lg-justify">--}}
                {{--<div class="cell-sm-10 cell-md-8 cell-lg-8">--}}
                    {{--<h4>DEVELOP YOUR COMPLEX DATA SYSTEM WITH OPEN SOURCE</h4>--}}
                    {{--<p>--}}
                        {{--OPEN SOURCE ENTERPRISE SOLUTIONS for Companies working with big volume dynamic data bases.--}}
                        {{--All in one - from backend to frontend.--}}
                    {{--</p><br>--}}

                    {{--<h6>Example:</h6>--}}
                    {{--<ul>--}}
                        {{--<li>Virtual servers - 350</li>--}}
                        {{--<li>System is divided in 3 vertical equal zones</li>--}}
                        {{--<li>Files’ volume in object storage - 80 Tb. Increasing + 3,5 Tb/month</li>--}}
                        {{--<li>Volume of NoSQL productive data base - 230 Gb</li>--}}
                        {{--<li>200 000 system users</li>--}}
                        {{--<li>Quantity of fixed sessions per day - 21 mln.</li>--}}
                        {{--<li>Traffic per day: 442 Gb downloaded, 88 Gb uploaded</li>--}}
                    {{--</ul>--}}
                    {{--<p>--}}
                        {{--SYSTEM OF GENERAL MONITORING, ALERT, LOGGING, RESERVATION and LOADS BALANCING guarantees Your safety and replaces usual SLAs of Vendors.--}}
                    {{--</p><br>--}}
                    {{--<h6>Example:</h6>--}}
                    {{--<p> Monitoring system provides quick response thanks to ready-made scripts and settings, graphs, dashboards, as well as alerts, configured for triggers by system levels and services.--}}
                        {{--Logging system ensures transparency of all changes in the system and the possibility of complete audit.--}}
                        {{--Load balancing ensures functioning of systems by distributing large loads.--}}
                    {{--</p>--}}

                    {{--<h4>WHY OPEN SOURCE TECHNOLOGIES:</h4>--}}
                    {{--<p>--}}
                        {{--OPEN SOURCE is the safest software — a code is developed and audited by the largest number of specialists in the world.--}}
                    {{--</p>--}}
                    {{--<p>--}}
                        {{--OPEN SOURCE is transparent, unlike vendor solutions, all changes are traceable.--}}
                    {{--</p>--}}
                    {{--<p>--}}
                        {{--OPEN SOURCE is fast track to innovations in IT.--}}
                    {{--</p>--}}
                    {{--<p>--}}
                        {{--Implementation of OPEN SOURCE solutions stimulates development of customer’s internal team, preventing degradation of Your admins which is the case when taking vendor solution.--}}
                    {{--</p>--}}
                    {{--<p>--}}
                        {{--SUPPORT ON SITE — an in-house team/team on site better understands internal processes and needs, leading to less resources spent for development аnd resulting in to exactly the system Your business requires.--}}
                        {{--Open documentation — normally in vendor solutions a product is 50% of the price, another 50% is implementation, training/learning and documentation.--}}
                        {{--With open products a customer has a choice — to take or not to take professional support, unlike with vendor solutions, where paid support is normally obligatory.--}}
                        {{--Experience tells, that even if You have SLA, but a problem is not obvious, its solution is sold as additional option or development. With open source alike situations are much more transparent.--}}
                        {{--Price depends on complexity of necessary solutions and individual development needs, but in general Your economy may achieve 30-50% of alike vendor solution.--}}
                    {{--</p>--}}
                {{--</div>--}}
                {{--<div class="cell-sm-10 cell-md-4">--}}
                    {{--<div class="row grid-2">--}}
                        {{--<div class="col-xs-12"><img src="/site/images/photo_2018-09-23_12-14-44.jpg" alt="" width="273" height="214"/>--}}
                        {{--</div>--}}
                        {{--<div class="col-xs-6"><img src="/site/images/about-1-273x214.jpg" alt="" width="273" height="214"/><img src="/site/images/about-2-273x214.jpg" alt="" width="273" height="214"/>--}}
                        {{--</div>--}}
                        {{--<div class="col-xs-6"><img src="/site/images/about-3-273x451.jpg" alt="" width="273" height="451"/>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

    </section>
@endsection
