<?php
/**
 * User: Andrey Naumoff
 * Date: 05-Mar-19
 * Time: 11:07 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace Modules\CRM\App\Services;

use Modules\CRM\App\Contracts\MultitenancyContract;

class MultitenancyService implements MultitenancyContract
{
    #region MAIN METHODS
    public function setCRMConnection(): void
    {
        config([
            'database.connections.crm' => [
                'driver' => 'mysql',
                'host' => env('DB_HOST', '127.0.0.1'),
                'port' => env('DB_PORT', '3306'),
                'database' => env('DB_DATABASE', 'forge'),
                'username' => env('DB_USERNAME', 'forge'),
                'password' => env('DB_PASSWORD', ''),
                'unix_socket' => env('DB_SOCKET', ''),
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
            ]
        ]);
    }
    #endregion
}
