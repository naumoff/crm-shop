<?php

namespace Modules\CRM;

use Illuminate\Support\ServiceProvider;
use Modules\CRM\App\Contracts\MultitenancyContract;
use Modules\CRM\App\Services\MultitenancyService;

class ModuleServiceProvider extends ServiceProvider
{
    #region PROPERTIES
    /**
     * All of the container singletons that should be registered.
     * @var array

     */
    public $singletons = [
        MultitenancyContract::class => MultitenancyService::class,
    ];
    #endregion

    #region MAIN METHODS
    /**
     * Bootstrap services.
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/crm.php');
    }

    /**
     * Register services.
     * @return void
     */
    public function register()
    {
        //
    }
    #endregion
}
