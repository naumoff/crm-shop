<?php
/**
 * User: Andrey Naumoff
 * Date: 05-Mar-19
 * Time: 10:10 PM
 * E-mail: andrey.naumoff@gmail.com
 */

$namespace = 'Modules\CRM\App\Http\Contollers';

Route::group(
    [
        'namespace' => $namespace,
        'prefix' => 'crm',
        'middleware' => ['web', 'bindings', 'auth', 'approved']
    ],
    function () {
        Route::get('/', function (\Modules\CRM\App\Contracts\MultitenancyContract $contract) {
            dump('Hello World!');
            dump(config('database.connections.crm'));
            $contract->setCRMConnection();
            dump(config('database.connections.crm'));
        });
    }
);
